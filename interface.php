<?php

require 'vendor/autoload.php';
use GeoIp2\Database\Reader;

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    return $ipaddress;
}


function get_country_name() {
    $reader = new Reader(__DIR__.'/GeoLite2-Country_20180102/GeoLite2-Country.mmdb');
    $record = $reader->country(get_client_ip());

    return strtolower($record->raw['registered_country']['names']['en']);
}
